//CSE hw02: Arithmetic Calculations 
//Yuting Sun 
public class Arithmetic{
  public static void main(String args[]){
    
    int numPants = 3; //Number of pairs of pants
    double pantsPrice = 34.98; //Cost per pair of pants
    
    int numShirts = 2; //Number of shirts
    double shirtPrice = 24.99; //Cost per shirts
    
    int numBelts = 1; //Number of belts
    double beltCost = 33.99; //Cost per belts
    
    double paSalesTax = 0.06; //the tax rate
    double totalCostOfPants; //total cost of pants
    double totalCostOfShirts; //total cost of shirts
    double totalCostOfBelts; //total cost of belts
    
   //Tax rate
    double SalesTaxOfPants; //sales tax of pants
    double SalesTaxOfShirts; //sales tax of shirts
    double SalesTaxOfBelts; //sales tax of belts
    
    
    double totalCostBeTax; //total cost before tax
    double totalSalesTax; //total sales tax
    double totalSales; //total sales
    
    totalCostOfPants = numPants * pantsPrice; //compute total cost of pants before tax
    totalCostOfShirts = numShirts * shirtPrice; //compute total cost of shirts before tax
    totalCostOfBelts = numBelts * beltCost; //compute total cost of belts before tax
    
    SalesTaxOfPants = ((int)totalCostOfPants * paSalesTax * 100) / 100; //compute sales tax of pants
    SalesTaxOfShirts = ((int)totalCostOfShirts * paSalesTax * 100)/ 100; //compute sales tax of shirts
    SalesTaxOfBelts = ((int)totalCostOfBelts * paSalesTax * 100) / 100; //compute sales tax of belts
    
    totalCostBeTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; //compute the total cost before tax
    totalSalesTax = ((int)totalCostBeTax * paSalesTax * 100) / 100; //compute the total sales tax
    totalSales = totalCostBeTax + totalSalesTax; //compute the total sales
    
    System.out.println("total cost of pants before tax is " + totalCostOfPants); //print out total cost before tax
    System.out.println("total cost of shirts before tax is " + totalCostOfShirts);
    System.out.println("total cost of belts before tax is " + totalCostOfBelts);
    
    System.out.println("total sale tax of pants is " + SalesTaxOfPants); //print out the sale tax of pants
    System.out.println("total sale tax of shirts is " + SalesTaxOfShirts); //print out the sale tax of shirts
    System.out.println("total sale tax of belts is " + SalesTaxOfBelts); //print out the sale tax of belts
    
    System.out.println("total cost before tax is " + totalCostBeTax); //print out the total cost before tax
    System.out.println("total sales tax is " + totalSalesTax); //print out the total sales tax
    System.out.println("total cost of the purchases is " + totalSales); //print out the total sales
    
    //lol
  }
  
  
  
}