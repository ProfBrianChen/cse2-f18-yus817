//CSE 002 hw03 calculate the volume inside the pyramid
//Yuting Sun
import java.util.Scanner;

public class Pyramid{
  public static void main(String args[]){
    
  
   Scanner scnr = new Scanner(System.in);//scanner constructor
   System.out.print("The square side of the pyramid is (input length) : ");//print out statement
   int sqSide = scnr.nextInt();//accepting user input
   double sqArea = Math.pow(sqSide,2);
    
   System.out.print("The height of the pyramid is (input height): ");//print out statement
   int pyramidHeight = scnr.nextInt();//accepting user input in inches
   
  //print output
    double pyramidVol;
    pyramidVol = (sqArea * pyramidHeight)/3;
    System.out.println("The volume inside the pyramid is: " + pyramidVol);
  }//end of main methods
  
}//end of main class
//cool!