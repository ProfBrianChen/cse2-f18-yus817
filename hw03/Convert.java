//CSE 002 hw03 convert the quantity of rain into cubic miles
//Yuting Sun
import java.util.Scanner;

public class Convert{
  public static void main(String args[]){
    
  
   Scanner scnr = new Scanner(System.in);//scanner constructor
   System.out.print("Enter the affected area in acres: ");//print out statement
   double affectedArea = scnr.nextDouble();//accepting user input
   affectedArea *= 0.00156;//convert acres to miles squared 
   
   System.out.print("Enter the rainfall in the affected area: ");
   double rainFall = scnr.nextDouble();//accepting user input in inches
   rainFall *= 1.578e-5;//convert inches to miles
    
  //print output
    double cubicMiles;
    cubicMiles = affectedArea * rainFall;//calculations
    System.out.println(cubicMiles + " cubic miles");
  }//end of main methods
  
}//end of main class
//lol