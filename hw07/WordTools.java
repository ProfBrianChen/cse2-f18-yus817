/*Yuting Sun CSE002
 * Hw07 Word Tools
 */
import java.util.Scanner;

public class WordTools {
  
  public static void main(String args[]) {
    String enterText = "";
    char enterChar = '<';
    String enterSearch = "";
    enterText = sampleText(enterText);//call method sampleText()
    
    while (enterChar != 'c' && enterChar != 'w' && enterChar != 'f' && 
           enterChar != 'r' && enterChar != 's' && enterChar != 'q') {
    enterChar = printMenu(enterChar);  
    if (enterChar == 'q') {
      break;
    }
    else if (enterChar == 'c') {
      System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(enterText));
    }
    else if (enterChar == 'w') {
      System.out.print("Number of words: " + getNumOfWord(enterText));
    }
    else if (enterChar == 'f') {
      findText(enterText, enterSearch);
    }
    else if (enterChar == 'r') {
      System.out.println(replaceExclamation(enterText));
    }
    else if (enterChar == 's') {
      shortenSpace(enterText);
    }
    }
  }
  
  public static String sampleText(String enterText) {//Output the string
    Scanner scnr1 = new Scanner(System.in);
    System.out.println("Enter a sample text: ");
    enterText = scnr1.nextLine();
    System.out.println("You entered: " + enterText);
    return enterText;//enter the string
  }
  
 public static char printMenu(char enterChar) {//print the menu
    Scanner scnr2 = new Scanner(System.in);
    System.out.println("MENU");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println("Choose an option: ");
    enterChar = scnr2.next().charAt(0);
    while ((enterChar != 'c') && (enterChar != 'w' ) && enterChar != 'f' && 
           enterChar != 'r' && enterChar != 's' && enterChar != 'q') {
      System.out.println("MENU");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println("Choose an option: ");
    enterChar = scnr2.next().charAt(0);
    }
    
    return enterChar;    
 }   
 
 public static int getNumOfNonWSCharacters(String enterText) {//3 count the non whitespaces
   int countWhitespace = enterText.length()- enterText.replaceAll(" ", "").length();
   int getNumOfNonWSCharacters = enterText.length();
   getNumOfNonWSCharacters = getNumOfNonWSCharacters - countWhitespace;
   return getNumOfNonWSCharacters;
 }
 
 public static int getNumOfWord(String enterText) {//4 number of words
   int getNumOfWord = 0;
   boolean word = false;
    int endOfLine = enterText.length();
    for (int i = 0; i < enterText.length(); i++) {
        // if the char is a letter, word = true.
        if (Character.isLetter(enterText.charAt(i)) && i != endOfLine) {
            word = true;
            // if char isn't a letter and there have been letters before,
            // counter goes up.
        } else if (!Character.isLetter(enterText.charAt(i)) && word) {
            getNumOfWord++;
            word = false;
            // last word of String; if it doesn't end with a non letter, it
            // wouldn't count without this.
        } else if (Character.isLetter(enterText.charAt(i)) && i == endOfLine) {
            getNumOfWord++;
        }
    }
    return getNumOfWord;
 }

 
 public static int findText(String enterText, String enterSearch) {//5 find text
   Scanner scnr2 = new Scanner(System.in);
   int findText = 0;

   System.out.println("Enter a word or phase to be found: ");
   enterSearch = scnr2.next();
   int j = 0;
   for (int i = 0; i <= enterSearch.length(); i++) {
       if ((enterSearch.charAt(i) == enterText.charAt(j))) {
         j++;
           if(j == enterSearch.length()) {       
           findText++;
           j = 0;
           }
         }
     }
   
 
   return findText;
 }

 public static String replaceExclamation(String enterText) {//6 replace !
   String replaceExclamation = enterText.replaceAll("!",".");
   return replaceExclamation;
 }
 public static String shortenSpace(String enterText) {//7 shorten space
   System.out.println(enterText.trim().replaceAll("\\s{2,}", " "));
   return enterText;
 }
}
