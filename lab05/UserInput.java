//CSE 002 lab05 Reading erroneous inputs
//Yuting Sun
import java.util.Scanner;
public class UserInput{
  public static void main(String args[]){
    Scanner scnr = new Scanner(System.in);
    System.out.println("Enter the course number: ");//question 1
    int courseNum;
    boolean isNum = false;
    while (isNum == false) {
      isNum = scnr.hasNextInt();
    if(isNum == true) {
        courseNum = scnr.nextInt();
        System.out.println("Course Number: " + courseNum);
      }
      else {
        System.out.println("Enter the course Number: ");
        isNum = false;
        scnr.next();
      }//end of if else
    }//end of while loop
    
    String departName;
    boolean isString = false;
    System.out.println("Enter the department Name: ");//question 2
    while (isString == false) {
      isString = scnr.hasNext();
    if(isString == true) {
        departName = scnr.next();
        System.out.println("Department Name: " + departName);
      }
      else {
        System.out.println("Enter the department Name: ");
        //isString = false;
        scnr.next();
      }//end of if else
    }//end of while loop
    
    
    int meetTimes;
    boolean isNum2 = false;
  System.out.println("Enter the number of times it meets in a week: ");//question 3
    while (isNum2 == false) {
      isNum2 = scnr.hasNextInt();
    if(isNum2 == true) {
        meetTimes = scnr.nextInt();
        System.out.println("The number of times it meets in a week: " + meetTimes);
      }
      else {
        System.out.println("Enter the number of times it meets in a week: ");
        scnr.next();//kick out the unwanted string
      }//end of if else
    }//end of while loop
    
    double startTime;
    boolean isNum3 = false;
    System.out.println("Enter the time the class starts: ");//question startTime
    while (isNum3 == false) {
      isNum3 = scnr.hasNextDouble();
    if(isNum3 == true) {
        startTime = scnr.nextDouble();
        System.out.println("The time the class starts: " + startTime);
      }
      else {
        System.out.println("Enter the number of times it meets in a week: ");
        scnr.next();//kick out the unwanted string
      }//end of if else
    }//end of while loop
      
    String instrName;
    boolean isString2 = false;
    System.out.println("Enter the Instructor Name: ");//question 5
    while (isString2 == false) {
      isString2 = scnr.hasNext();
    if(isString2 == true) {
       instrName = scnr.next();
        System.out.println("Instructor Name: " + instrName);
      }
      else {
        System.out.println("Enter the Instructor Name: ");
        scnr.next();
      }//end of if else
    }//end of while loop
    
    int numStudents;
    boolean isNum4 = false;
    System.out.println("Enter the number of students: ");//question 6
    while (isNum4 == false) {
      isNum4 = scnr.hasNextInt();
    if(isNum4 == true) {
        numStudents = scnr.nextInt();
        System.out.println("The number of students: " + numStudents);
      }
      else {
        System.out.println("Enter the number of students: ");
        scnr.next();//kick out the unwanted string
      }//end of if else
    }//end of while loop
    
  }
  
}