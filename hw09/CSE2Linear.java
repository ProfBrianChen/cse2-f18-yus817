//Yuting Sun
//CSE002 homework09
import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;
public class CSE2Linear {
  public static void main(String args[]) {
  int [] gradeS = new int [15];
  int num = 1000;
  System.out.println("Enter 15 ascending ints for final grades in CSE2:");
  Scanner scnr = new Scanner(System.in);
  
  for (int i = 0; i < gradeS.length; i++) {
   boolean isInt = false;
   boolean isRange = false;
   boolean isGreater = false;
   if (i == 0) {//test for the first element
   while (isInt == false && isRange == false) {
   isInt = scnr.hasNextInt();
   if (isInt == true) {
     num = scnr.nextInt();
   }
   else {
     System.out.println("enter Ints");
     scnr.next();
     i = 0;
   }
   if (num <= 100 && num >= 0) {
     isRange = true;
     gradeS[0] = num;
   }
   else {
     System.out.println("enter 0-100");
     scnr.next();
     i = 0;
   }
   }
   }
   else {//test for the remaining elements
     while (isInt == false && isRange == false && isGreater == false) {
    isInt = scnr.hasNextInt();
   if (isInt == true) {
     num = scnr.nextInt();
   }
   else {
     System.out.println("enter Ints");
     scnr.next();
     i--;
   }
   if (num <= 100 && num >= 0) {
     isRange = true;
     gradeS[i] = num;
   }
   else {
     System.out.println("enter 0-100");
     //scnr.next();
     i--;
   }
   if (gradeS[i] >= gradeS[i - 1]) {
     isGreater = true;
     gradeS[i] = num;
   }
   else {
     System.out.println("Enter asending order");
     //scnr.next();
     i--;
   }
 
  }
  } 
  }
  System.out.println(Arrays.toString(gradeS)); 
  System.out.println("search the grade: ");
  int search = scnr.nextInt();
  System.out.println("number was found in " + binarySearch(gradeS,search) + " iterations");//binarySearch
  
  System.out.println("search the grade again: ");
  int search1 = scnr.nextInt();
  int[] ranScram = randomScramble(gradeS);//scramble process
    System.out.println(Arrays.toString(ranScram)); 
  System.out.println("number was found in " + linearSearch(ranScram,search1) + " iterations");
  }
  public static int binarySearch (int[] list, int target) {
    int first = 0;
    int last = list.length - 1;
    int iterations = 0;
    while (last >= first) {
      int middle = ( first + last) / 2;
      if (target < list[middle]) {
        last = middle - 1;
        iterations++;
      }
      else if (target == list[middle]) {
        iterations++;
        return middle;
      }
      else {
        first = middle + 1;
        iterations++;
      }
    } 
    return iterations;
  }
  
  public static int linearSearch(int[] list, int target) {
    for ( int i = 0; i < list.length; i++) {
      if (target == list[i] ) return i;
    }
    return -1;
  }
  public static int[] randomScramble (int[] list) {
    Random ranGen = new Random();
    int [] randomScramble = new int [15];
    int first = list[0];
    int tempNum = 0;
    for (int i = 0; i < list.length; i++) {//shuffle 15 times
      int j = ranGen.nextInt(15);
      tempNum = first;
      first = list[j];
      list[j] = tempNum;
    }
    return list;
  }

}