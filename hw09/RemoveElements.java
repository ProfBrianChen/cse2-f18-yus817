//Yuting Sun 
//CSE 002 Hw9 Remove Elements
import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;
public class RemoveElements{
  public static void main(String [] arg){
  Scanner scan = new Scanner(System.in);
  int num[]=new int[10];
  int newArray1[];
  int newArray2[];
  int index,target;
  String answer="";
 do{
   System.out.print("Random input 10 ints [0-9]");
   num = randomInput();
   String out = "The original array is:";
   out += listArray(num);
   System.out.println(out);
 
   System.out.print("Enter the index ");
   index = scan.nextInt();
   newArray1 = delete(num,index);
   String out1="The output array is ";
   out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
   System.out.println(out1);

   System.out.print("Enter the target value ");
   target = scan.nextInt();
   newArray2 = remove(num,target);
   String out2="The output array is ";
   out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
   System.out.println(out2);
    
   System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
   answer=scan.next();
  }while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
  String out="{";
  for(int j = 0;j < num.length;j++){
   if(j>0){
     out += ", ";
   }
   out += num[j];
  }
  out += "} ";
  return out;
  }

  public static int[] randomInput() {
    Random ranGen = new Random();
    int randomInput [] = new int [10];
    for (int i = 0; i < randomInput.length; i++) {
      randomInput[i] = ranGen.nextInt(10);
    }
    return randomInput;
  }
  public static int[] delete(int[] list, int pos) {
    Scanner scan = new Scanner(System.in);
    int[] delete = new int [9];
    boolean checkNum = true;
    while (checkNum) {
    if ( pos < 10 && pos >= 0) {
      for ( int i = 0; i < pos; i++) {
        delete[i] = list[i];
      }
      for (int j = pos; j < 9; j++) {
        delete[j] = list[j + 1];
      }
      checkNum = false;
    }
      else {
        System.out.println("out of bounds, enter 0-9");
        pos = scan.nextInt();
        checkNum = true;
      }
    }
    return delete;
  }
  public static int[] remove(int[] list, int target) {
    int count = 0;
    for (int j = 0; j < list.length; j++) {
      if (list[j] == target) {
        count++;
      }
    }
    System.out.println("count: " + count);
    int[] remove = new int [list.length - count];
    int j = 0;
    for (int i = 0; i < list.length; i++) {
      if (list[i] != target && j < (list.length - count)) {
        remove[j] = list[i];  
        j++;
      }
    }
     return remove;
  }
}