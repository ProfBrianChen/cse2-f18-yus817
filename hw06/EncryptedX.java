/*Yuting Sun hw06 CSE002
 * EncrytedX
 */
import java.util.Scanner;

public class EncryptedX {
  
  public static void main(String args[]) {
      
    Scanner scnr = new Scanner (System.in);
    boolean enterNum = true;//enter while loop
    int num = 0;
    
    System.out.println("Enter the number between 1 and 100: ");//enter the side length of square 
    
    while (enterNum) {
      if (scnr.hasNextInt()) {
      num = scnr.nextInt();
      
      if ( num <= 0 || num >= 101) {
        System.out.println("Enter the number between 1 and 100: ");
        }
      else {
        if (num != 11 && num != 22 && num != 33 && num != 44 && num != 55 && num != 66 && num != 77 &&
      num != 88 && num != 99) {
        System.out.println("cannot form a square");//smallest square is 11*11
        }
        else {
        enterNum = false;
        System.out.println("num = " + num);
      }
      }
      }
    else {
  scnr.next();
  System.out.println("Enter the number between 1 and 100: ");
}
 
}//end of while loop number 
    //number is the side length of the square
    int numCol = 0;//number of columns
    int numLine = 0;//number of lines
    for ( numLine = 0; numLine <= num - 1; numLine++) {//number of lines
    for ( numCol = 0; numCol <= num - 2; numCol++) {//number of columns
      
        if (numCol == numLine || numLine == num - numCol - 1) {
        System.out.print(" ");//print blankspace
        }
       else {
          System.out.print("*");//print stars 
        }
    }//number of column end
        if (numLine == numCol || numCol == num - numLine - 1) {
        System.out.println(" ");//print blankspace
        }
        else {
          System.out.println("*");//print stars 
    }
    }//number of lines end
   
    
      
}//end of the method
}//lol end of the program
