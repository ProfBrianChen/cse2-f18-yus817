//CSE 002 hw04 classic casino games Craps using if-else
//Yuting Sun
import java.util.Random;
import java.util.Scanner;
public class CrapsIf{
  public static void main(String args[]) {
    Scanner scnr = new Scanner(System.in);
    int typeOfPlay;//0 means randomly cast dice and 1 means state the two dice
    
    int dice1 = 1;//value of dice1
    int dice2 = 1;//value of dice2
    int diceVal = 1;//value of dice1 + dice2
    
    System.out.println("random play enter 0");
    System.out.println("state two dice enter 1");
    typeOfPlay = scnr.nextInt();//user enter the type of play
    
    if(typeOfPlay == 0) {
      Random ranGen = new Random();
      dice1 = ranGen.nextInt(6)+1;
      dice2 = ranGen.nextInt(6)+1;
      diceVal = dice1 + dice2;
      System.out.println("dice1 value: " + dice1);
      System.out.println("dice1 + dice2 value: " + diceVal);
      
    }
  else if(typeOfPlay == 1) {
    System.out.println("Enter your dice values: ");
    dice1 = scnr.nextInt();//enter value for the dice1
    dice2 = scnr.nextInt();
    if (dice1 > 6 || dice2 > 6 || dice1 < 1 || dice2 < 1) {
      System.out.println("Please enter valid values");
    }
    else{
      diceVal = dice1 + dice2;
      System.out.println(dice1 + dice2);//print out dice1 + dice2
    }
    
  }
    else {
      System.out.println("Please enter either 0 or 1");
    }
    
    //determine the slang terminology of the outcome of the roll
    if (diceVal == 2) {
      System.out.println("Snake Eyes");
    }
    else if (diceVal == 3) {
      System.out.println("Ace Deuce");
    }
    else if (diceVal == 4) {
      System.out.println("Easy Four");
    }
    else if (diceVal == 5) {
      System.out.println("Fever Five");
    }  
    else if (diceVal == 6) {
      System.out.println("Easy Six");
    }
    else if (diceVal == 7) {
      System.out.println("Seven out");
    }
    else if (diceVal == 8) {
       System.out.println("Easy Eight");  
       }
    else if (diceVal == 9) {
       System.out.println("Nine");  
       }
    else if (diceVal == 10) {
       System.out.println("Easy Ten");  
       }
    else if (diceVal == 11) {
       System.out.println("Yo-leven");  
       }
    else if (diceVal == 12) {
       System.out.println("Boxcars");  
       }
    else {
      System.out.println("Invaild slang");
    }
    
    
    
  }//end of main class
  
}//end of main program