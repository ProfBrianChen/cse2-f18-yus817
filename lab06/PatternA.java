//Yuting Sun
//CSE PatternA
import java.util.Scanner;
public class PatternA {
  public static void main(String args[]) {
    Scanner scnr = new Scanner(System.in);
    int numLine = 0;
    boolean enterNum = true;
    
    System.out.println("Enter 1 to 10: "); //How many lines to generate
 
    while (enterNum) {
      if (scnr.hasNextInt()) {
     numLine = scnr.nextInt();

     if (numLine <= 0 || numLine >= 11) {
      System.out.println("Enter again: ");
     }
 
     else{
       enterNum = false;
       System.out.println("Lines: " + numLine);
     }
      }
      else {
        scnr.next();
        System.out.println("Enter again: ");
      }
    }
     
  
 for (int numRow = 1; numRow <= numLine; numRow++) {
   System.out.println(" ");
   for (int num = 1; num <= numRow; num++) {
     System.out.print(num + " ");
   }
   }
 
 
 
  
  /* ADD YOUR CODE HERE */
  
}//main class
  
}//end of program
