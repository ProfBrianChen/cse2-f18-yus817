//Yuting Sun
//CSE 002 lab06 PatternD
import java.util.Scanner;
public class PatternD {
  public static void main(String args[]) {
    Scanner scnr = new Scanner(System.in);
    int numLine = 0;//initialize variable
    boolean enterNum = true;//enter the while loop
    
    System.out.println("Enter 1 to 10: "); //How many lines to generate
 
    while (enterNum) {
      if (scnr.hasNextInt()) {
     numLine = scnr.nextInt();

     if (numLine <= 0 || numLine >= 11) {
      System.out.println("Enter again: ");//test if number are out of range
     }
 
     else{
       enterNum = false;
       System.out.println("Lines: " + numLine);
     }
      }
      else {
        scnr.next();
        System.out.println("Enter again: ");
      }
    }
   
 for (int numRow = numLine; numRow >= 1; numRow--) {
   for (int num = numRow; num >= 1; num--) {
     System.out.print(num + " ");//4321
   }
 System.out.println("");//next line
 }

   }
   }//end of the program


  

