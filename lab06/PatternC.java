//CSE 002 lab06 PatternC
//Yuting Sun
import java.util.Scanner;
public class PatternC {
  public static void main(String args[]) {
    Scanner scnr = new Scanner(System.in);
    int numLine = 0;//initialize variable
    boolean enterNum = true;//enter the while loop
    String result = "";

    System.out.println("Enter 1 to 10: "); //How many lines to generate
 
    while (enterNum) {
      if (scnr.hasNextInt()) {
     numLine = scnr.nextInt();

     if (numLine <= 0 || numLine >= 11) {
      System.out.println("Enter again: ");//test if number are out of range
     }
 
     else{
       enterNum = false;
       System.out.println("Lines: " + numLine);
     }
      }
      else {
        scnr.next();
        System.out.println("Enter again: ");
      }
    }


for(int numRow = 1; numRow <= numLine; numRow++){
    result = numRow + result;
      System.out.printf("%11s\n",result);
}


}//end of the method


}//end of the program

