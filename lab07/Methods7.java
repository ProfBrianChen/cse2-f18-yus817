//Yuting Sun CSE002 lab07 Methods
import java.util.Random;
import java.util.Scanner;
public class Methods7{    
public static String aDj() {//adjectives
  Random ranGen = new Random();//create the Random object
  int num = ranGen.nextInt(10);
  //System.out.println("num is: " + num);
  String aDJ = "";
  switch (num) {
    case 0:
      aDJ = "powerful";
      break;
    case 1:
      aDJ = "impressive";
      break;
    case 2:
      aDJ = "simple";
      break;
    case 3:
      aDJ = "brilliant";
      break;
    case 4:
      aDJ = "fast";
      break;
    case 5:
      aDJ = "advance";
      break;
    case 6:
      aDJ = "expensive";
      break;
    case 7:
      aDJ = "cheap";
      break;
    case 8:
      aDJ = "important";
      break;
    case 9:
      aDJ = "slow";
      break;
  }
  return aDJ;
}
public static String sUbj() {//subject of a sentence
  Random ranGen = new Random();//create the Random object
  int num = ranGen.nextInt(10);
  //System.out.println("num is: " + num);
  String sUb = "";
  switch (num) {
    case 0:
      sUb = "Apple";
      break;
    case 1:
      sUb = "Microsoft";
      break;
    case 2:
      sUb = "Engineer";
      break;
    case 3:
      sUb = "designer";
      break;
    case 4:
      sUb = "ebay";
      break;
    case 5:
      sUb = "facebook";
      break;
    case 6:
      sUb = "google";
      break;
    case 7:
      sUb = "programmer";
      break;
    case 8:
      sUb = "consumers";
      break;
    case 9:
      sUb = "investors";
      break;
  }
  return sUb;
}
   
public static String pAstV() {//past-tense verbs
  Random ranGen = new Random();//create the Random object
  int num = ranGen.nextInt(10);
  //System.out.print("num is: " + num);
  String pV = "";
  switch (num) {
    case 0:
      pV = "redesigned";
      break;
    case 1:
      pV = "packed";
      break;
    case 2:
      pV = "engineered";
      break;
    case 3:
      pV = "made";
      break;
    case 4:
      pV = "used";
      break;
    case 5:
      pV = "achieved";
      break;
    case 6:
      pV = "equipped";
      break;
    case 7:
      pV = "machined";
      break;
    case 8:
      pV = "began";
      break;
    case 9:
      pV = "built";
      break;
  }
  return pV;
}
public static String oBj() {//object of the sentence
  Random ranGen = new Random();//create the Random object
  int num = ranGen.nextInt(10);
  //System.out.print("num is: " + num);
  String oBj = "";
  switch (num) {
    case 0:
      oBj = "Mac";
      break;
    case 1:
      oBj = "connectors";
      break;
    case 2:
      oBj = "computers";
      break;
    case 3:
      oBj = "display";
      break;
    case 4:
      oBj = "pixels";
      break;
    case 5:
      oBj = "keyboard";
      break;
    case 6:
      oBj = "screen";
      break;
    case 7:
      oBj = "camera";
      break;
    case 8:
      oBj = "senors";
      break;
    case 9:
      oBj = "storage";
      break;
  }
  return oBj;
}

public static String sentence1(String subject1){//create the thesis sentence
  String sentence1 = aDj() + " " + subject1 + " " + pAstV() + " " + aDj() + " " + oBj() + "."; 
  return sentence1;
}
public static String sentence2 (String subject1) {//action sentence
 String sentence2 = subject1 + " " + pAstV() + " "+ aDj() + " " +oBj();
 return sentence2;
}
public static String conclusionS (String subject1) {//conclusion sentence
 String conclusionS = subject1 + " " + pAstV() + " " + oBj();
 return conclusionS;
}
public static String paragraph1 (String subject1) {
   Random ranGen = new Random();//create the Random object
   int numOfSentences = ranGen.nextInt(10) + 1;
  System.out.println("num of sentences: " + numOfSentences);
   String paragraph1 = sentence1(subject1);
   
   for (int i = 0; i <= numOfSentences; i++) {
     paragraph1 += sentence2(subject1) + ". ";
   }
   paragraph1 += "The " + conclusionS(subject1) + "!";
   return paragraph1;
}
  
public static void main (String args[]) {
  System.out.println("Phase 1");
  Scanner scnr = new Scanner (System.in);
  String subject1 = sUbj();
  System.out.println(aDj() + " " + subject1 + " " + pAstV() + " " + oBj() + ".");
  boolean aNother = true;
  while (aNother) {
  System.out.println("Enter 1 to have a new sentence, other number to quit");
  int bInary = scnr.nextInt();
  if (bInary == 1) {
    aNother = true;
    System.out.println(sentence1(subject1));
  }
  else {
    aNother = false;
  }
  }
  //create a paragraph
  System.out.println("Phase 2");
  System.out.println(paragraph1(subject1));
  
}//end of main class
}
  
  