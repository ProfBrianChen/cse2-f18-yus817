//Yuting Sun
//CSE 002 Hw08 shuffling
import java.util.Scanner;
import java.util.Random;
public class Shuffling {
  
  public static void main (String args[]) {
 Scanner scnr = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    
 String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
 String[] cards = new String[52]; 
 String[] hand = new String[5]; 
 int numCards = 5; 
 int again = 1; 
 int index = 51;
for (int i = 0; i < 52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); 
} 
System.out.println();
printArray(cards); 
shuffle(cards); 
System.out.println("-----");
System.out.println("shuffled");
printArray(cards);

while(again == 1){ 
   hand = getHand(cards,index,numCards); 
   System.out.println("Hand");
   printArray(hand);
   index = index - numCards;
   if (numCards > index) {
     shuffle(cards);//create a new deck of cards
     index = 51;
   }
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scnr.nextInt(); 
}  

  }//end of main methods

public static void printArray(String[] cards) {//print cards
  for (int i = 0; i < cards.length; i++){ 
  System.out.print(cards[i]+" "); //print arrays
} 
  System.out.println();
}

public static String shuffle(String[] cards) {//shuffle cards
   Random ranGen = new Random();
  String firstCard = cards[0];//first card
  String tempCard = "";
  for(int i = 0; i < 51; i++) {//shuffle 51 times
  int j = ranGen.nextInt(51) + 1; 
  tempCard = firstCard;//swap cards
  firstCard = cards[j];
  cards[j] = tempCard;
  }
  
  return firstCard;
}

public static String[] getHand(String[] cards, int index, int numCards) {
  String[] getHand = new String [numCards];//get five cards to form one hand
  for (int i = 0; i < numCards; i++) {
  getHand[i] = cards[index - i];
  }
  return getHand;
}
  

  
}//end of the program
