Grading Sheet for HW8

Grade: 100

Compiles    				          20 pts
Comments 				              10 pts
Method shuffle(list)			      20 pts

Method getHand(list,index, numCards)  20 pts

Method printArray(list).              20 pts

Checks if numCards given > number of cards, if so creates a new deck of cards.   10 pts