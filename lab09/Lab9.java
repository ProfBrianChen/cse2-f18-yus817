//Yuting Sun
//CSE002 lab9 passing arrays inside methods
import java.util.Arrays;
public class Lab9 {
  public static void main(String args[]) {
    final int [] array0 = {1,2,3,4,5,6,7,8};
    int [] array1 = copy(array0);
    int [] array2 = copy(array0);
  
  print(inverter(array0));//Pass array0 to inverter() and print it out with print()
  System.out.println();
  print(inverterTwo(array1));//Pass array1 to inverter2() and print it out with print()
  System.out.println();
  int [] array3 = inverterTwo(array2);
  print(array3);
  System.out.println();
  /*System.out.println("tests: " + Arrays.toString(array0));
  System.out.println(Arrays.toString(array1));
  System.out.println(Arrays.toString(array3));*/
  }
  
  public static int[] copy(int[] array0) {
    int [] arrayTwo = new int [8];
    for (int i = 0; i < array0.length; i++) {
      arrayTwo[i] = array0[i];//copy each member in the array
    }
    return arrayTwo;
  }
  public static int [] inverter(int[] array0) {//reserses the order of an array
    for (int i = 0; i < (array0.length / 2); i++) {
      int tempVal = array0[i];
      array0[i] = array0[array0.length - 1 - i];
      array0[array0.length - 1 - i] = tempVal;
  }
    /*for (int i = 0; i < arrayOne.length; i++) {
      System.out.print(arrayOne[i] + " ");
    }*/
    return array0;
  
}
  public static int [] inverterTwo(int [] array0) {//inverter two
    int [] arrayThre = copy(array0);
    inverter(arrayThre);
    return arrayThre;
  }
    
  public static void print(int[] array0) {//print
    for (int i = 0; i < array0.length; i++) {
      System.out.print(array0[i] + " ");
    }
  }
    
}//end of program
