//CSE 002 lab03 Yuting Sun
//the program to help spliting the check and calculating percentage of tip
import java.util.Scanner;

public class Check{
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner( System.in ); //Scanner constructor
    System.out.print("Enter the oringinal cost of the check in the form xx.xx: ");
    double checkCost = myScanner.nextDouble(); //accepting user input
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; //convert the percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
//print out the output
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies;
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    dollars = (int)costPerPerson;
    dimes = (int)(costPerPerson * 10) % 10; 
    pennies = (int)(costPerPerson * 100) %10;
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);
    
  } //end of main methods
} //end of class